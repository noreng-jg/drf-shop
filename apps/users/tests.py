from django.test import TestCase, Client
from rest_framework.authtoken.models import Token
from .models import UserProfile, User
from .views import list_users_view
from rest_framework import status
from rest_framework.test import force_authenticate, APIRequestFactory

client = Client()
factory = APIRequestFactory()

class CreateUserTest(TestCase):
  def setUp(self):
     UserProfile.objects.create(username='user', email='email@email.com', password='senha')
  def test_email_required(self):
    response = client.post('/users/register', {'username': 'user1', 'password':'password'})
    self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
    self.assertEqual(response.data['non_field_errors'][0].__str__(), 'Email is required.')

  def test_fullname_required(self):
    response = client.post('/users/register', {'username': 'user1', 'password':'password', 'email': 'email@email.com'})
    self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
    self.assertEqual(response.data['non_field_errors'][0].__str__(), 'Fullname is required.')

  def test_email_exists(self):
    response = client.post('/users/register', {'username': 'user1', 'password':'password', 'email': 'email@email.com', 'fullname': 'fullname'})
    self.assertEqual(response.status_code, status.HTTP_409_CONFLICT)
    self.assertEqual(response.data, 'Email already taken.')

  def test_username_exists(self):
    #default django error
    response = client.post('/users/register', {'username': 'user', 'password':'password', 'email': 'email2@email.com', 'fullname': 'fullname'})
    self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
    self.assertEqual(response.data['username'][0], 'A user with that username already exists.')

  def test_user_creatd(self):
    response = client.post('/users/register', {'username': 'user2', 'password':'password', 'email': 'email2@email.com', 'fullname': 'fullname'})
    self.assertEqual(response.status_code, status.HTTP_201_CREATED)
    self.assertEqual(response.data.get('password'), None)

class ListUser(TestCase):
  def setUp(self):
   # create super user
    User.objects.create_superuser(username='useradmin', email='superadmin@email', password='senha')

    UserProfile.objects.create(username='user', email='email@email.com', password='senha')
    UserProfile.objects.create(username='user2', email='email2@email.com', password='senha')

  def test_list_users_forbidden(self):
    response = client.get('/users')
    self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

  def test_list_users_authenticated(self):
    request = factory.get('/users')
    force_authenticate(request, user=User.objects.get(username='useradmin'))
    response = list_users_view(request)
    self.assertEqual(response.status_code, status.HTTP_200_OK)
    self.assertEqual(len(response.data), 2)
    self.assertEqual(response.data[0].get('username'), 'user')
    self.assertEqual(response.data[1].get('username'), 'user2')

class LoginUser(TestCase):
  def setUp(self):
    UserProfile.objects.create_user(username='user', email='email@email.com', password='senha')

  def test_login_fails(self):
    response = client.post('/users/login', {'login': 'user1', 'password':'password'})
    self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

  def test_login_username_success(self):
    response = client.post('/users/login', {'login': 'user', 'password':'senha'})
    self.assertEqual(response.status_code, status.HTTP_200_OK)
    self.assertTrue(response.data.get('token'))

  def test_login_email_success(self):
    response = client.post('/users/login', {'login': 'email@email.com', 'password':'senha'})
    self.assertEqual(response.status_code, status.HTTP_200_OK)
    self.assertTrue(response.data.get('token'))
