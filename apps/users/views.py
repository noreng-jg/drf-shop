from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.decorators import permission_classes, api_view, authentication_classes
from rest_framework.permissions import IsAuthenticated, IsAdminUser, AllowAny
from rest_framework.authentication import BasicAuthentication
from rest_framework.authtoken.models import Token
from rest_framework import serializers 
from rest_framework.exceptions import ValidationError
from .models import UserProfile
from .serializers import RegisterUserSerializer, LoginSerializer, ListUsersSerializer

@api_view(['GET'])
@permission_classes([IsAdminUser])
@authentication_classes([BasicAuthentication])
def list_users_view(request, format=None):
    users = UserProfile.objects.all()
    serializer = ListUsersSerializer(users, many=True)
    return Response(serializer.data)

class RegisterUserView(APIView):
  permission_classes = [AllowAny]

  def post(self, request, format=None):
    try:
        serializer = RegisterUserSerializer(data=request.data)

        if serializer.is_valid():
            serializer.save()
            data = serializer.data.copy()
            data.pop('password')
            return Response(data, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    except Exception as e:
        if e.__class__.__name__ == 'ValidationError':
            return Response(e.detail[0], status=status.HTTP_409_CONFLICT)

        return Response({"error": str(e)}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

class AuthUserView(APIView):
    permission_classes = [AllowAny]
    """
        Return a token for the user
    """
    def post(self, request, format=None):
        try:
            serializer = LoginSerializer(data=request.data)

            if serializer.is_valid():
                user = serializer.validated_data['user']
                token, created = Token.objects.get_or_create(user=user)
                return Response({'token': token.key}, status=status.HTTP_200_OK)

            return Response(serializer.errors, status=status.HTTP_401_UNAUTHORIZED)

        except Exception as e:
            return Response({"error": str(e)}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
