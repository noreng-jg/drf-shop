from django.contrib.auth.models import User
from rest_framework import serializers
from django.contrib.auth import authenticate
from datetime import datetime
from .models import UserProfile

class ListUsersSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserProfile
        fields = ('id', 'username', 'email', 'fullname', 'created_at')

class LoginSerializer(serializers.Serializer):
    login = serializers.CharField(max_length=255)
    password = serializers.CharField(max_length=128, write_only=True)

    class Meta:
        fields = ('login', 'password')

    def validate(self, data):
        """
        attempts to login either by username or email address
        """

        login = data.get('login')
        password = data.get('password')

        if login and password:
            user = UserProfile.objects.filter(username=login).first()

            if user is not None:
                user_data = authenticate(username=user.username, password=password)
                data['user'] = user_data
                return data
            else:
                user = UserProfile.objects.filter(email=login).first()
                
                if user is not None:
                    user_data = authenticate(username=user.username, password=password)
                    data['user'] = user_data
                    return data

            raise serializers.ValidationError('Incorrect login or password')
        else:
            raise serializers.ValidationError('A login and password are required.')

class RegisterUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserProfile
        fields = ('username', 'password', 'email', 'fullname')

    # validataion to ensure that the email and fullname are not blank
    def validate(self, data):
        # check if the email is provided and not blank
        if not 'email' in data :
            raise serializers.ValidationError("Email is required.")
        if not 'fullname' in data:
            raise serializers.ValidationError("Fullname is required.")
        return data

    def create(self, validated_data):
        if UserProfile.objects.filter(email=validated_data['email']).exists():
            raise serializers.ValidationError("Email already taken.")

        if UserProfile.objects.filter(username=validated_data['username']).exists():
            raise serializers.ValidationError("Username already taken.")

        user = UserProfile.objects.create_user(**validated_data)
        return user
