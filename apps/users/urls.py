from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns
from users import views

urlpatterns = [
  path('users', views.list_users_view, name='list_users'),
  path('users/login', views.AuthUserView.as_view()),
  path('users/register', views.RegisterUserView.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)
