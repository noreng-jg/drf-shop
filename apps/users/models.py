from django.db import models
from django.contrib.auth.models import User

class UserProfile(User):
    user_ptr = models.OneToOneField(User, parent_link=True, primary_key=True, on_delete=models.CASCADE)
    fullname = models.CharField(max_length=100, default='')
    created_at = models.DateTimeField(auto_now_add=True)

    # Override the __str__() method to return something meaningful
    def __str__(self):
        return self.username

    # Override the delete() method to delete the user's profile
    def delete(self, *args, **kwargs):
        self.user.delete()
        super(UserProfile, self).delete(*args, **kwargs)
