from rest_framework import serializers
from .models import Product, Business, Tag

class GetProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = ('id', 'name', 'price', 'description', 'quantity', 'business', 'tags')
        read_only_fields = ('id',)

    def to_representation(self, instance):
        ret = super().to_representation(instance)
        ret['tags'] = [tag.name for tag in instance.tags.all()]
        b_fields = [ 'id', 'name', 'description', 'field']
        ret['business'] = {name: getattr(instance.business, name) for name in b_fields}
        return ret

class CreateProductSerializer(serializers.ModelSerializer):
    tags = serializers.ListField(child=serializers.CharField(), required=False, allow_null=True, write_only=True)

    class Meta:
        model = Product
        fields = ('name', 'price', 'description', 'quantity', 'tags')

    def validate(self, data):
        if data.get('name') is None:
            raise serializers.ValidationError("Name is required")
        if data.get('price') is None:
            raise serializers.ValidationError("Price is required")
        if data.get('description') is None:
            raise serializers.ValidationError("Description is required")
        if data.get('quantity') is None:
            raise serializers.ValidationError("Quantity is required")
        if data['quantity'] < 0:
            raise serializers.ValidationError("Quantity must be greater than 0")
        return data

    def create(self, validated_data):
        if 'tags' in validated_data:
            tags = validated_data.pop('tags')
            product = Product.objects.create(**validated_data)

            for tag in tags:
                tag_obj = Tag.objects.get_or_create(name=tag)
                product.tags.add(tag_obj[0])

            return product

        product = Product.objects.create(**validated_data)
        return product
