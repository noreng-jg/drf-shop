from django.db import models
from business.models import Business

class Product(models.Model):
    # Create a one-to-many relationship with the Business model
    business = models.ForeignKey(Business, on_delete=models.CASCADE)
    name = models.CharField(max_length=100, default='')
    description = models.CharField(max_length=100, default='')
    price = models.FloatField(default=0)
    quantity = models.IntegerField(default=0)
    tags = models.ManyToManyField('Tag', blank=True, related_name='products')
    # Override the __str__() method to return something meaningful
    def __str__(self):
        return self.name

class Tag(models.Model):
    name = models.CharField(max_length=100, default='')

    # Override the __str__() method to return something meaningful
    def __str__(self):
        return self.name
