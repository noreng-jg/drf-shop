from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns
from products import views

urlpatterns = [
    path('products', views.get_products, name='get_products'),
    path('products/<int:pk>', views.ProductDetailsView.as_view()),
    path('business/<int:pk>/product', views.create_product, name='create_product'),
]

urlpatterns = format_suffix_patterns(urlpatterns)
