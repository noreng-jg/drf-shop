from rest_framework import serializers
from rest_framework.views import APIView
from rest_framework.decorators import api_view, permission_classes, authentication_classes
from rest_framework import status
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from .models import Product, Business
from .serializers import GetProductSerializer, CreateProductSerializer
from .permissions import BusinessOwnsProduct, OwnsBusiness

@api_view(['GET'])
@permission_classes([AllowAny])
@authentication_classes([])
def get_products(request):
    try:
        products = Product.objects.all()
        serializer = GetProductSerializer(products, many=True)
        return Response(serializer.data)

    # caught generic exception
    except Exception as e:
        return Response({"message": str(e)}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

@api_view(['POST'])
@permission_classes([OwnsBusiness])
def create_product(request, pk):
    try:
        business = Business.objects.get(id=pk)
        serializer = CreateProductSerializer(data=request.data)

        if serializer.is_valid():
            product = serializer.save(business=business)
            return Response(product, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    # caught generic exception
    except Exception as e:
        if e.__class__.__name__ == 'ValidationError':
            return Response({"message": str(e)}, status=status.HTTP_400_BAD_REQUEST)

        return Response({"message": str(e)}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

class ProductDetailsView(APIView):
    permission_classes = [BusinessOwnsProduct]

    def get(self, request, pk):
        try:
            product = Product.objects.get(id=pk)
            serializer = GetProductSerializer(product)
            return Response(serializer.data)

        # caught generic exception
        except Exception as e:
            return Response({"message": str(e)}, status=status.HTTP_404_NOT_FOUND)

    def put(self, request, pk):
        try:
            product = Product.objects.get(id=pk)
            serializer = CreateProductSerializer(product, data=request.data)

            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=status.HTTP_200_OK)

            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        # caught generic exception
        except Exception as e:
            if e.__class__.__name__ == 'ValidationError':
                return Response({"message": str(e)}, status=status.HTTP_400_BAD_REQUEST)

            return Response({"message": str(e)}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    def delete(self, request, pk):
        try:
            product = Product.objects.get(id=pk)
            product.delete()
            return Response(status=status.HTTP_204_NO_CONTENT)
        except Exception as e:
            return Response({"message": str(e)}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
