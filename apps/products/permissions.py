#extend basepermission
from rest_framework import permissions
from .models import Product, Business
from django.http import Http404

class BusinessOwnsProduct(permissions.BasePermission):
    def has_permission(self, request, view):
        if request.method == 'GET':
            return True

        if request.method in ['PUT', 'DELETE']:
            try:
                product = Product.objects.get(id=view.kwargs['pk'])

                if product == None:
                    return False

                return product.business.user == request.user
            except Exception as e:
                raise Http404

        return False

class OwnsBusiness(permissions.BasePermission):
    def has_permission(self, request, view):
        if request.method == 'POST':
            business = Business.objects.get(id=view.kwargs['pk'])

            if business is None:
                return False

            return business.user == request.user

