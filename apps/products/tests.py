from django.test import TestCase, Client
from rest_framework.authtoken.models import Token
from .models import Business, Product, Tag
from django.contrib.auth.models import User
from .views import get_products, create_product, ProductDetailsView
from rest_framework import status
from rest_framework.test import force_authenticate, APIRequestFactory
from rest_framework.authtoken.models import Token

client = Client()
factory = APIRequestFactory()

class GetProductsTest(TestCase):
    def setUp(self):
        self.user = User.objects.create_user(username='user', password='pass', email='admin@email.com')
        self.business1 = Business.objects.create(
            name='Test Business',
            description='Test description',
            field='Test field',
            user_id=self.user.id
        )

        self.product1 = Product.objects.create(
            id = 1,
            name='Test Product',
            price=10,
            description='Test description',
            quantity=10,
            business_id=self.business1.id
        )

        self.product2 = Product.objects.create(
            id = 2,
            name='Test Product 2',
            price=10,
            description='Test description',
            quantity=10,
            business_id=self.business1.id
        )

        self.product3 = Product.objects.create(
            id = 3,
            name='Test Product 3',
            price=10,
            description='Test description',
            quantity=10,
            business_id=self.business1.id
        )


    def test_list_products(self):
        response = client.get('/products')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 3)

    def test_list_product(self):
        response = client.get('/products/1')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['name'], 'Test Product')
        self.assertEqual(response.data['price'], 10)
        self.assertEqual(response.data['description'], 'Test description')
        self.assertEqual(response.data['quantity'], 10)

class CreateProductTest(TestCase):
    def setUp(self):
        self.user = User.objects.create_user(username='user', password='pass', email='email@email.com')
        self.user2 = User.objects.create_user(username='user2', password='pass', email='email2@email.com')
        self.token = Token.objects.create(user=self.user)
        self.token2 = Token.objects.create(user=self.user2)
        self.tag = Tag.objects.create(name='Test Tag')

        self.business1 = Business.objects.create(
            id =1,
            name='Test Business',
            description='Test description',
            field='Test field',
            user_id=self.user.id
        )

        self.business2 = Business.objects.create(
           id = 2,
            name='Test Business 2',
            description='Test description',
            field='Test field',
            user_id=self.user.id
        )

    def test_create_product_fail_unauthorized(self):
        data4 = {
            'name': 'Test Product',
            'price': 10,
            'description': 'Test description',
            'quantity': 10,
        }

        response = client.post('/business/1/product', data=data4)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_create_product_forbidden(self):
        data3 = {
            'name': 'Test Product',
            'price': 10,
            'description': 'Test description',
            'quantity': 10,
        }

        request = factory.post('/business/1/product', data=data3)
        force_authenticate(request, user=self.user2)
        response = create_product(request, pk=1)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(response.data['detail'], 'You do not have permission to perform this action.')


    def test_create_product_fail(self):
        data = {
            'name': '',
            'price': 10,
            'description': 'Test description',
            'quantity': 10,
        }

        request = factory.post('/business/1/product', data=data)
        force_authenticate(request, user=self.user)
        response = create_product(request, pk=1)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_create_product_succeed(self):
        data2 = {
            'name': 'Test Product 1290',
            'price': 10,
            'description': 'Test description',
            'quantity': 10,
            'tags': ['Test Tag']
        }

        request = factory.post('/business/1/product', data=data2)
        force_authenticate(request, user=self.user)
        response = create_product(request, pk=1)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        
class ProductDetailsTest(TestCase):
    def setUp(self):
        self.user = User.objects.create_user(username='user', password='pass', email='admin@email.com')
        self.user2 = User.objects.create_user(username='user2', password='pass', email='email2@email.conm')

        self.business1 = Business.objects.create(
            name='Test Business',
            description='Test description',
            field='Test field',
            user_id=self.user.id
        )

        self.product1 = Product.objects.create(
            id = 1,
            name='Test Product',
            price=10,
            description='Test description',
            quantity=10,
            business_id=self.business1.id
        )

        self.product2 = Product.objects.create(
            id = 2,
            name='Test Product 2',
            price=10,
            description='Test description',
            quantity=10,
            business_id=self.business1.id
        )

        self.product3 = Product.objects.create(
            id = 3,
            name='Test Product 3',
            price=10,
            description='Test description',
            quantity=10,
            business_id=self.business1.id
        )

        self.business2 = Business.objects.create(
            name='Test Business 2',
            description='Test description',
            field='Test field',
            user_id=self.user2.id
        )

    def test_product_update_details_unauthorized(self):
        response = client.put('/products/1')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_product_update_details_forbidden(self):
        request = factory.put('/products/1')
        force_authenticate(request, user=self.user2)
        response = ProductDetailsView.as_view()(request, pk=1)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(response.data['detail'], 'You do not have permission to perform this action.')

    def test_product_delete_details_unauthorized(self):
        response = client.delete('/products/1')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_product_delete_details_forbidden(self):
        request = factory.delete('/products/1')
        force_authenticate(request, user=self.user2)
        response = ProductDetailsView.as_view()(request, pk=1)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(response.data['detail'], 'You do not have permission to perform this action.')

    def test_product_update_details_success(self):
        data = {
            'name': 'Test Product 3',
            'price': 20,
            'description': 'Test description 3',
            'quantity': 12,
        }

        request = factory.put('/products/1', data=data)
        force_authenticate(request, user=self.user)
        response = ProductDetailsView.as_view()(request, pk=1)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['name'], 'Test Product 3')
        self.assertEqual(response.data['price'], 20)
        self.assertEqual(response.data['description'], 'Test description 3')
        self.assertEqual(response.data['quantity'], 12)

    def test_product_delete_details_success(self):
        request = factory.delete('/products/1')
        force_authenticate(request, user=self.user)
        response = ProductDetailsView.as_view()(request, pk=1)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(response.data, None)

    def test_product_delete_not_found(self):
        request = factory.delete('/products/4')
        force_authenticate(request, user=self.user)
        response = ProductDetailsView.as_view()(request, pk=4)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.assertEqual(response.data['detail'], 'Not found.')

