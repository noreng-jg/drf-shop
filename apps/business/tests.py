from django.test import TestCase, Client
from rest_framework.authtoken.models import Token
from .models import Business
from .models import User
from .views import list_businesses, BusinessDetailsView, create_business
from rest_framework import status
from rest_framework.test import force_authenticate, APIRequestFactory
# Create your tests here.

client = Client()
factory = APIRequestFactory()

class ListBusinessesTest(TestCase):
    def setUp(self):
        self.admin = User.objects.create_superuser(username='user', password='pass', email='admin@email.com')
        self.business1 = Business.objects.create(
            name='Test Business',
            description='Test description',
            field='Test field',
            user_id=self.admin.id
        )

        self.business2 = Business.objects.create(
            name='Test Business 2',
            description='Test description 2',
            field='Test field 2',
            user_id=self.admin.id
        )

    def test_list_forbidden(self):
        response = client.get('/business')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_list_businesses(self):
        request = factory.get('/business')
        force_authenticate(request, user = self.admin)
        response = list_businesses(request)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 2)

class BusinessDetailsViewTest(TestCase):
    def setUp(self):
        self.user = User.objects.create_user(username='user', password='pass', email='user1@email.com')
        self.user2 = User.objects.create_user(username='user2', password='pass', email='user2@email.com')

        self.business1 = Business.objects.create(
            id = 10,
            name='Test Business',
            description='Test description',
            field='Test field',
            user_id=self.user.id
        )

        self.business2 = Business.objects.create(
            id = 20,
            name='Test Business 2',
            description='Test description 2',
            field='Test field 2',
            user_id=self.user2.id
        )

    def test_business_details_requires_authentication(self):
        response = client.get('/business/10')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_business_update_details_success(self):
        request = factory.put('/business/10', {'name': 'Test Business', 'description': 'Test description', 'field': 'Test field'})
        force_authenticate(request, user = self.user)
        response = BusinessDetailsView.as_view()(request, pk=10)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['name'], 'Test Business')
        self.assertEqual(response.data['description'], 'Test description')
        self.assertEqual(response.data['field'], 'Test field')

    def test_business_update_details_forbidden(self):
        request = factory.put('/business/10', {'name': 'Test Business', 'description': 'Test description', 'field': 'Test field'})
        force_authenticate(request, user = self.user2)
        response = BusinessDetailsView.as_view()(request, pk=10)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_business_delete_details_success(self):
        request = factory.delete('/business/10', {'name': 'Test Business', 'description': 'Test description', 'field': 'Test field'})
        force_authenticate(request, user = self.user)
        response = BusinessDetailsView.as_view()(request, pk=10)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(Business.objects.filter(user=self.user).count(), 0)

class CreateBusinessTest(TestCase):
    def setUp(self):
        self.user = User.objects.create_user(username='user', password='pass', email='emial@email.com')

        self.business1 = Business.objects.create(
            name='Test Business',
            description='Test description',
            field='Test field',
            user_id=self.user.id
        )

    def test_create_business_requires_authentication(self):
        response = client.post('/business/create', {'name': 'Test Business', 'description': 'Test description', 'field': 'Test field'})
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_create_business_returns_invalid(self):
        request = factory.post('/business/create', {'name': '', 'description': '', 'field': ''})
        force_authenticate(request, user = self.user)
        response = create_business(request)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_create_business_success(self):
        request = factory.post('/business/create', {'name': 'Test Business', 'description': 'Test description', 'field': 'Test field'})
        force_authenticate(request, user = self.user)
        response = create_business(request)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Business.objects.count(), 2)
        self.assertEqual(Business.objects.get(id=2).name, 'Test Business')
        self.assertEqual(Business.objects.get(id=2).description, 'Test description')
        self.assertEqual(Business.objects.get(id=2).field, 'Test field')
