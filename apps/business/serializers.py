from rest_framework import serializers
from .models import Business

class ListBusinessSerializer(serializers.ModelSerializer):
    class Meta:
        model = Business
        fields = ('id', 'user', 'name', 'field', 'description')

class CreateBusinessSerializer(serializers.ModelSerializer):
    class Meta:
        model = Business
        fields = ('name', 'field', 'description')

    def validate(self, data):
        if not data.get('name'):
            raise serializers.ValidationError("Name is required")
        if not data.get('field'):
            raise serializers.ValidationError("Field is required")
        if data['name'] == '':
            raise serializers.ValidationError("Name cannot be empty")
        if data['field'] == '':
            raise serializers.ValidationError("Field cannot be empty")
        return data

    def create(self, validated_data):
        return Business.objects.create(**validated_data)

class UpdateBusinesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Business
        fields = ('name', 'field', 'description', 'id')
        # read only id
        read_only_fields = ('id',)

    def validate(self, data):
        if not data.get('name'):
            raise serializers.ValidationError("Name is required")
        if not data.get('field'):
            raise serializers.ValidationError("Field is required")
        if data['name'] == '':
            raise serializers.ValidationError("Name cannot be empty")
        if data['field'] == '':
            raise serializers.ValidationError("Field cannot be empty")
        return data

    def update(self, instance, validated_data):
        instance.name = validated_data.get('name', instance.name)
        instance.field = validated_data.get('field', instance.field)
        instance.description = validated_data.get('description', instance.description)
        instance.save()
        return instance
