from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns
from business import views

urlpatterns = [
  path('business', views.list_businesses, name='list_businesses'),
  path('business/create', views.create_business, name='create_business'),
  path('business/<int:pk>', views.BusinessDetailsView.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)
