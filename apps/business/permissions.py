#import basepermission
from rest_framework import permissions
from .models import Business

class OwnsBusiness(permissions.BasePermission):
    def has_permission(self, request, view):
        return Business.objects.filter(user=request.user, id=view.kwargs['pk']).count() > 0
