from django.db import models
from django.contrib.auth.models import User

class Business(models.Model):
    # Create a one-to-many relationship with the User model
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=100, default='')
    field = models.CharField(max_length=100, default='')
    description = models.CharField(max_length=100, default='')

    # Override the __str__() method to return something meaningful
    def __str__(self):
        return self.name
