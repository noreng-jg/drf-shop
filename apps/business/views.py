from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.permissions import IsAdminUser, IsAuthenticated
from rest_framework.decorators import api_view, permission_classes, authentication_classes
from rest_framework.authentication import BasicAuthentication
from rest_framework.views import APIView
from .serializers import ListBusinessSerializer, CreateBusinessSerializer, UpdateBusinesSerializer
from .permissions import OwnsBusiness
from .models import Business

@api_view(['GET'])
@permission_classes([IsAdminUser])
@authentication_classes([BasicAuthentication])
def list_businesses(request, format=None):
    businesses = Business.objects.all()
    serializers = ListBusinessSerializer(businesses, many=True)
    return Response(serializers.data)

@api_view(['POST'])
@permission_classes([IsAuthenticated])
def create_business(request, format=None):
    try:
        serializer = CreateBusinessSerializer(data=request.data)

        if serializer.is_valid():
            serializer.save(user=request.user)
            return Response({}, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    except Exception as e:
        print(e.__class__.__name__)
        if e.__class__.__name__ == "ValidationError":
            return Response(e.detail[0], status=status.HTTP_400_BAD_REQUEST)

        return Response({"error": str(e)}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

class BusinessDetailsView(APIView):
    permission_classes = [IsAuthenticated, OwnsBusiness]

    def delete(self, request, pk, format=None):
        try:
            business = Business.objects.get(pk=pk)
            business.delete()
            return Response(status=status.HTTP_204_NO_CONTENT)
        except Business.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)
    
    def put(self, request, pk, format=None):
        try:
            business = Business.objects.get(pk=pk)
            serializer = UpdateBusinesSerializer(business, data=request.data)

            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data)

            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        except Business.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)
